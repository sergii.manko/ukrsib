CREATE SEQUENCE public.hibernate_sequence;
DROP TABLE IF EXISTS "transaction" CASCADE;

CREATE TABLE "transaction"
(
  id        BIGSERIAL    NOT NULL PRIMARY KEY,
  place     VARCHAR(255) NOT NULL,
  amount    NUMERIC      NOT NULL,
  currency  VARCHAR(255) NOT NULL,
  card      VARCHAR(255) NOT NULL,
  client_id BIGINT
)
