package com.uksibbank.xmlparser.service;

import com.uksibbank.xmlparser.domain.Client;
import com.uksibbank.xmlparser.domain.Transaction;
import com.uksibbank.xmlparser.repository.ClientRepository;
import com.uksibbank.xmlparser.repository.TransactionRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;
import org.xml.sax.SAXException;

import javax.transaction.Transactional;
import javax.xml.XMLConstants;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.transform.stax.StAXSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Service
@Log4j2
public class TransactionService {
    private static final String VALIDATE_SCHEMA = "classpath:text.xsd";
    private final TransactionRepository transactionRepository;
    private final ClientRepository clientRepository;
    @Value("${batch_size}")
    private int batchSize;

    public TransactionService(TransactionRepository transactionRepository, ClientRepository clientRepository) {
        this.transactionRepository = transactionRepository;
        this.clientRepository = clientRepository;
    }

    @Transactional
    public void saveAll(final List<Transaction> transactions) {
        transactionRepository.saveAll(transactions);
    }

    /**
     * <p>Method for read data from file,parse it and store in the database</p>
     */
    public void parseTransaction(final String filePath) throws XMLStreamException {
        try {
            validateDocument(filePath);
            final XMLInputFactory factory = XMLInputFactory.newInstance();
            final File file = ResourceUtils.getFile(filePath);
            final XMLStreamReader reader = factory.createXMLStreamReader(new FileInputStream(file));
            reader.nextTag();
            log.info("Start processing data from file: {}", file.getAbsolutePath());
            readRoot(reader);
            log.info("End processing data from file: {}", file.getAbsolutePath());
        } catch (IOException e) {
            log.info("File " + filePath + " not found!");
        }
    }

    /**
     * <p>Method for read data from soap stream while node is transaction</p>
     *
     * @param reader XMLStreamReader
     */

    private void readRoot(final XMLStreamReader reader) throws XMLStreamException {
        while (reader.nextTag() == XMLStreamConstants.START_ELEMENT) {
            if (isTagTransactions(reader.getLocalName())) {
                readTransactions(reader);
            }
        }
    }

    /**
     * <p>Method for read data from soap stream and save it to database</p>
     *
     * @param reader XMLStreamReader
     */
    private void readTransactions(final XMLStreamReader reader) throws XMLStreamException {
        final List<Transaction> transactionList = new ArrayList<>();
        while (reader.nextTag() == XMLStreamConstants.START_ELEMENT) {
            if (!isTagTransaction(reader.getLocalName())) {
                break;
            }
            saveClientBeforeTransaction(reader, transactionList);
            saveBatchTransactions(transactionList);
            reader.nextTag();
        }
        saveAll(transactionList);
    }

    private void saveClientBeforeTransaction(final XMLStreamReader reader, final List<Transaction> transactionList)
            throws XMLStreamException {
        final Transaction transaction = readTransaction(reader);
        final Client client = transaction.getClient();
        if (checkClientOnUnique(client.getInn())) {
            clientRepository.saveAndFlush(transaction.getClient());
        }
        transaction.setClient(clientRepository.getByInn(client.getInn()));
        transactionList.add(transaction);
    }

    /**
     * <p>Method for put data into the database in batches</p>
     *
     * @param transactionList List<Transaction>
     */
    private void saveBatchTransactions(final List<Transaction> transactionList) {
        if (transactionList.size() >= batchSize) {
            saveAll(transactionList);
            transactionList.clear();
        }
    }

    /**
     * <p>Method for read data from soap stream and parse it to Transaction entity</p>
     *
     * @param reader XMLStreamReader
     * @return the Transaction entity
     */
    private Transaction readTransaction(final XMLStreamReader reader) throws XMLStreamException {
        final Transaction transaction = new Transaction();
        reader.nextTag();
        transaction.setPlace(reader.getElementText());
        reader.nextTag();
        transaction.setAmount(new BigDecimal(reader.getElementText()));
        reader.nextTag();
        transaction.setCurrency(reader.getElementText());
        reader.nextTag();
        transaction.setCard(reader.getElementText());
        final Client client = readClients(reader);
        setClient(transaction, client);
        return transaction;
    }

    private void setClient(final Transaction transaction, final Client client) {
        if (checkClientOnUnique(client.getInn())) {
            transaction.setClient(client);
        } else {
            Client clientByInn = clientRepository.getByInn(client.getInn());
            transaction.setClient(clientByInn);
        }
    }

    private boolean checkClientOnUnique(final String inn) {
        return clientRepository.getByInn(inn) == null;
    }

    /**
     * <p>Method for read data from soap stream while node is client</p>
     *
     * @param reader XMLStreamReader
     * @return the Client entity
     */
    private Client readClients(final XMLStreamReader reader) throws XMLStreamException {
        while (reader.nextTag() == XMLStreamConstants.START_ELEMENT) {
            if (isTagClient(reader.getLocalName())) {
                return readClient(reader);
            }
        }
        return null;
    }

    /**
     * <p>Method for read data from soap stream and parse it to Client entity</p>
     *
     * @param reader XMLStreamReader
     * @return the Client entity
     */
    private Client readClient(final XMLStreamReader reader) throws XMLStreamException {
        final Client client = new Client();
        reader.nextTag();
        client.setFirstName(reader.getElementText());
        reader.nextTag();
        client.setLastName(reader.getElementText());
        reader.nextTag();
        client.setMiddleName(reader.getElementText());
        reader.nextTag();
        client.setInn(reader.getElementText());
        reader.nextTag();
        return client;
    }

    private boolean isTagTransaction(final String tagName) {
        return "transaction".equals(tagName);
    }

    private boolean isTagTransactions(final String tagName) {
        return "transactions".equals(tagName);
    }

    private boolean isTagClient(final String tagName) {
        return "client".equals(tagName);
    }

    private void validateDocument(final String fileName) {
        final File file;
        try {
            file = ResourceUtils.getFile(fileName);

            final XMLStreamReader reader = XMLInputFactory.newInstance().createXMLStreamReader(
                    new FileInputStream(file));

            final SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            final File schemaFile = ResourceUtils.getFile(VALIDATE_SCHEMA);
            final Schema schema = factory.newSchema(schemaFile);

            final Validator validator = schema.newValidator();
            validator.validate(new StAXSource(reader));
        } catch (FileNotFoundException e) {
            log.info("File not found " + fileName);
        } catch (IOException e) {
            log.info("File " + fileName + " not found!");
        } catch (XMLStreamException e) {
            log.info("Error get xml stream from file " + fileName + " " + e.getMessage());
        } catch (SAXException e) {
            log.info("Parse file " + fileName + " error " + e.getMessage());
        }
        log.debug("File validation success!");
    }
}
