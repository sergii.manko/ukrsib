package com.uksibbank.xmlparser;

import com.uksibbank.xmlparser.service.TransactionService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;

@SpringBootApplication
@PropertySource("application-dev.yaml")
public class XMLParserApplication implements CommandLineRunner {
    private final TransactionService transactionService;

    public XMLParserApplication(final TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    public static void main(String[] args) {
        SpringApplication.run(XMLParserApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        final String filePath = "classpath:text.xml";
        transactionService.parseTransaction(filePath);
    }
}
