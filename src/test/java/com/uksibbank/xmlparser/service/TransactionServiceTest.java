package com.uksibbank.xmlparser.service;

import com.uksibbank.xmlparser.domain.Client;
import com.uksibbank.xmlparser.domain.Transaction;
import com.uksibbank.xmlparser.repository.ClientRepository;
import com.uksibbank.xmlparser.repository.TransactionRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.xml.stream.XMLStreamException;
import java.util.List;

import static org.junit.Assert.assertEquals;

@SpringBootTest
@RunWith(SpringRunner.class)
public class TransactionServiceTest {
    @Autowired
    private TransactionService transactionService;
    @Autowired
    private TransactionRepository transactionRepository;
    @Autowired
    private ClientRepository clientRepository;

    @Test
    public void parseTransaction() throws XMLStreamException {
        final String testFile = "classpath:text.xml";
        transactionService.parseTransaction(testFile);
        final List<Transaction> allTransactions = transactionRepository.findAll();
        final int sizeOfXmlFile = 24;
        final int sizeOfClientFromFile = 3;
        final List<Client> allClients = clientRepository.findAll();
        assertEquals(allTransactions.size(), sizeOfXmlFile);
        assertEquals(allClients.size(), sizeOfClientFromFile);
    }
}
